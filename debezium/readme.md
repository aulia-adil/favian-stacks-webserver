# Acquisition-Debezium

Docker configuration for Datastream Debezium instance. Administration of Debezium cluster are done through port `8083`.
## Run Container
`docker-compose up -d`

## Connector Administration
Shell scripts for Debezium connector administrations are available in the folder
- `list-connector.sh` list of connectors registered
- `delete-connector.sh` delete a specific connector
- `refresh-connector.sh` do a connector refresh by recreating it

## Replication Log Administration in PostgreSQL
### List of all replication slot registered in PostgreSQL
```SELECT * FROM pg_replication_slots```

### Delete replication slot (if new connector exceeds max replication slot)
```pg_drop_replication_slot('slotname')```